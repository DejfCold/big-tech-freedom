# Preface
This is in no way ordered by preference.

Subcategory is selected by the UI design as I cannot try everything

MR's welcome

# Requirements
- Federated
- Open source
- interop possibility

# Social networks
## Twitter
- Mastodon: https://joinmastodon.org/
- Diaspora: https://diasporafoundation.org/

## Facebook
- Friendica: https://friendi.ca/

## Instagram
- Pixelfed: https://pixelfed.org/

## Youtube
- PeerTube: https://joinpeertube.org/

# Chat
- Matrix: https://matrix.org/
- Signal: https://signal.org/ (ok, it's not federated, but at least it's an opensource)

# Maps
- OpenStreetMap: https://www.openstreetmap.org

# Web Search
Well, you can't really federate that, can you?
- DuckDuckGo: https://duckduckgo.com/

# Email
Email itself is federated, but since so many people are using gmail, let's step it up a bit. Let's encrypt our emails with ease. Sure, you could use pretty much any provider over eg. Thundrebird with [Enigmail](https://addons.thunderbird.net/en-US/thunderbird/addon/enigmail/), but that's not really user friendly and doesn't work on mobile.
- Protonmail: https://protonmail.ch

# File storage
Dropbox, OneDrive, Google drive, etc. alternative
- Nextcloud: https://nextcloud.com/

# Office suite
Online:
- OnlyOffice: https://www.onlyoffice.com/

Desktop:
- LibreOffice: https://www.libreoffice.org/

# News aggregation
- RSS: https://www.rssboard.org/rss-specification
- Atom: https://tools.ietf.org/html/rfc5023

# Banks and money
- Bitcoin: https://bitcoin.org

# DNS
Ok, I get it, it's an altcoin, you may not like it. But there's nothing like this on Bitcoin's blockchain afaik. It's also kinda useless as registrars will honor your free speech most of the time.
- Namecoin: https://www.namecoin.org/

# Shoutout
- https://switching.software/
